import React, { Component } from 'react';
import { View, Platform, Text, ScrollView, Image, StyleSheet, ToastAndroid } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import  NetInfo  from '@react-native-community/netinfo';
import { createDrawerNavigator, DrawerItemList } from '@react-navigation/drawer';
import Menu from './MenuComponent';
import DishDetail from './DishDetailComponent';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import Favorites from './FavoriteComponent';
import Reservation from './ReservationComponent';
import Login from './LoginComponent';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux';
import { fetchDishes, fetchComments, fetchLeaders, fetchPromos } from '../redux/ActionCreators';


const mapStateToProps = state => {
    return {

    }
}

const mapDispatchToProps = dispatch => ({

    fetchDishes: () => dispatch(fetchDishes()),
    fetchComments: () => dispatch(fetchComments()),
    fetchPromos: () => dispatch(fetchPromos()),
    fetchLeaders: () => dispatch(fetchLeaders())
})

const Stack = createStackNavigator();

function MenuNavigator({ navigation }) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
            initialRouteName="Menu"
        >
            <Stack.Screen name="Menu" component={Menu}
                options={{
                    headerLeft: () => (
                        <Icon name='menu'
                            size={24}
                            color='white'
                            onPress={() => navigation.toggleDrawer()}
                        />
                    )
                }}
            />
            <Stack.Screen name="DishDetail" component={DishDetail} />

        </Stack.Navigator>
    );
}

function HomeNavigator({ navigation }) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
            initialRouteName="Home"
        >
            <Stack.Screen name="Home" component={Home}
                options={{
                    headerLeft: () => (
                        <Icon name='menu'
                            size={24}
                            color='white'
                            onPress={() => navigation.toggleDrawer()}
                        />
                    )
                }}
            />
        </Stack.Navigator>
    );
}

function ContactNavigator({ navigation }) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
            initialRouteName="Contact"
        >
            <Stack.Screen name="Contact" component={Contact}
                options={{
                    headerLeft: () => (
                        <Icon name='menu'
                            size={24}
                            color='white'
                            onPress={() => navigation.toggleDrawer()}
                        />
                    )
                }}
            />
        </Stack.Navigator>
    );
}

function AboutNavigator({ navigation }) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
            initialRouteName="About"
        >
            <Stack.Screen name="About" component={About}
                options={{
                    headerLeft: () => (
                        <Icon name='menu'
                            size={24}
                            color='white'
                            onPress={() => navigation.toggleDrawer()}
                        />
                    )
                }}
            />
        </Stack.Navigator>
    );
}


function LoginNavigator({ navigation }) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
            initialRouteName="Login"
        >
            <Stack.Screen name="Login" component={Login}
                options={{
                    headerLeft: () => (
                        <Icon name='menu'
                            size={24}
                            color='white'
                            onPress={() => navigation.toggleDrawer()}
                        />
                    )
                }}
            />
        </Stack.Navigator>
    );
}

function ReservationNavigator({ navigation }) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
            initialRouteName="Reservation"
        >
            <Stack.Screen name="Reservation" component={Reservation}
                options={{
                    headerLeft: () => (
                        <Icon name='menu'
                            size={24}
                            color='white'
                            onPress={() => navigation.toggleDrawer()}
                        />
                    )
                }}
            />
        </Stack.Navigator>
    );
}

function FavoritesNavigator({ navigation }) {
    return (
        <Stack.Navigator
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
            initialRouteName="Favorites"
        >
            <Stack.Screen name="Favorites" component={Favorites}
                options={{
                    headerLeft: () => (
                        <Icon name='menu'
                            size={24}
                            color='white'
                            onPress={() => navigation.toggleDrawer()}
                        />
                    )
                }}
            />
            <Stack.Screen name="DishDetail" component={DishDetail} />
        </Stack.Navigator>
    );
}

const CustomDrawerContentComponent = (props) => (
    <ScrollView>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
            <View style={styles.drawerHeader}>
                <View style={{ flex: 1 }}>
                    <Image source={require('./images/logo.png')} style={styles.drawerImage} />
                </View>
                <View style={{ flex: 2 }}>
                    <Text style={styles.drawerHeaderText}>Ristorante Con Fusion</Text>
                </View>
            </View>
            <DrawerItemList  {...props} />
        </SafeAreaView>
    </ScrollView>
);
const Drawer = createDrawerNavigator();

class Main2 extends Component {
    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchComments();
        this.props.fetchLeaders();
        this.props.fetchPromos();

        NetInfo.fetch()
            .then((state ) => {
                ToastAndroid.show('Initial Network Connectivity Type: '
                    + state.type + ', effectiveType: ' + state .effectiveType,
                    ToastAndroid.LONG)
            });

        NetInfo.addEventListener(this.handleConnectivityChange);
    }

    componentWillUnmount() {
        NetInfo.addEventListener(this.handleConnectivityChange);
      }

    handleConnectivityChange = (state) => {
        switch (state.type) {
            case 'none':
                ToastAndroid.show('You are now offline!', ToastAndroid.LONG);
                break;
            case 'wifi':
                ToastAndroid.show('You are now connected to WiFi!', ToastAndroid.LONG);
                break;
            case 'cellular':
                ToastAndroid.show('You are now connected to Cellular!', ToastAndroid.LONG);
                break;
            case 'unknown':
                ToastAndroid.show('You now have unknown connection!', ToastAndroid.LONG);
                break;
            default:
                break;
        }
    }
    render() {
        return (
            <NavigationContainer>
                <Drawer.Navigator initialRouteName="Home"
                    drawerStyle={{
                        backgroundColor: '#D1C4E9'
                    }}
                    drawerContent={(props) => <CustomDrawerContentComponent {...props} />}
                >
                    <Drawer.Screen name="Menu" component={MenuNavigator}
                        options={() => ({
                            drawerIcon: ({ tintColor, focused }) => (
                                <Icon
                                    name='list'
                                    type='font-awesome'
                                    size={24}
                                    color={tintColor}
                                />
                            )
                        })}
                    />
                    <Drawer.Screen name="Home" component={HomeNavigator}
                        options={() => ({
                            drawerIcon: ({ tintColor, focused }) => (
                                <Icon
                                    name='home'
                                    type='font-awesome'
                                    size={24}
                                    color={tintColor}
                                />
                            )
                        })} />
                    <Drawer.Screen name="Contact us" component={ContactNavigator}
                        options={() => ({
                            drawerIcon: ({ tintColor, focused }) => (
                                <Icon
                                    name='address-card'
                                    type='font-awesome'
                                    size={22}
                                    color={tintColor}
                                />
                            )
                        })} />
                    <Drawer.Screen name="About us" component={AboutNavigator}
                        options={() => ({
                            drawerIcon: ({ tintColor, focused }) => (
                                <Icon
                                    name='info-circle'
                                    type='font-awesome'
                                    size={24}
                                    color={tintColor}
                                />
                            )
                        })} />
                    <Drawer.Screen name="My Favorites" component={FavoritesNavigator}
                        options={() => ({
                            drawerIcon: ({ tintColor, focused }) => (
                                <Icon
                                    name='heart'
                                    type='font-awesome'
                                    size={22}
                                    color={tintColor}
                                />
                            )
                        })} />
                    <Drawer.Screen name="Reserve Table" component={ReservationNavigator}
                        options={() => ({
                            drawerIcon: ({ tintColor, focused }) => (
                                <Icon
                                    name='cutlery'
                                    type='font-awesome'
                                    size={22}
                                    color={tintColor}
                                />
                            )
                        })} />
                    <Drawer.Screen name="Login" component={LoginNavigator}
                        options={() => ({
                            drawerIcon: ({ tintColor, focused }) => (
                                <Icon
                                    name='sign-in'
                                    type='font-awesome'
                                    size={24}
                                    color={tintColor}
                                />
                            )
                        })} />
                </Drawer.Navigator>
            </NavigationContainer>

        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    drawerHeader: {
        backgroundColor: '#512DA8',
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row'
    },
    drawerHeaderText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    },
    drawerImage: {
        margin: 10,
        width: 80,
        height: 60
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(Main2);

