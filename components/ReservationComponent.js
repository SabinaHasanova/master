import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, Switch, Button, Modal, Alert } from 'react-native';
import { Icon } from 'react-native-elements';
import { Picker } from '@react-native-picker/picker';
import Moment from 'moment';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import * as Animatable from 'react-native-animatable';
import * as Permissions from 'expo-permissions';
import { Notifications } from 'expo';
import * as Calendar from 'expo-calendar';

class Reservation extends Component {
    constructor(props) {
        super(props);

        this.state = {
            guests: 1,
            smoking: false,
            date: new Date(),
            showDateTimePicker: false,
            showModal: false
        }
    }

    static navigationOptions = {
        title: 'Reserve Table',
    };

    handleReservation() {

        Alert.alert(
            'Your Reservation OK?',
            'Number of Guests : ' + this.state.guests + '\n' +
            'Smoking? ' + this.state.smoking + '\n' +
            'Date and time : ' + Moment(this.state.date).format('lll') + '\n',
            [
                { text: 'Cancel', onPress: () => { this.resetForm() }, style: 'cancel' },
                {
                    text: 'OK', onPress: () => {
                        this.presentLocalNotification(this.state.date);
                        this.addReservationToCalendar(this.state.date);
                        this.resetForm();
                    }
                },
            ],
            { cancelable: false }
        )


    }

    onChangeDateTimePicker = value => {
        this.setState({
            date: value,
            showDateTimePicker: false,
        });

    };

    resetForm() {
        this.setState({
            guests: 1,
            smoking: false,
            date: new Date(),
            showModal: false,
            showDateTimePicker: false
        });
    }

    toggleDateTimePicker = () => {
        this.setState({
            showDateTimePicker: !this.state.showDateTimePicker,
        });
    }
    toggleModal = () => {
        this.setState({ showModal: !this.state.showModal });
    }

    async obtainNotificationPermission() {
        let permission = await Permissions.getAsync(Permissions.USER_FACING_NOTIFICATIONS);
        if (permission.status !== 'granted') {
            permission = await Permissions.askAsync(Permissions.USER_FACING_NOTIFICATIONS);
            if (permission.status !== 'granted') {
                Alert.alert('Permission not granted to show notifications');
            }
        }
        return permission;
    }

    async obtainCalendarPermission() {
        let permission = await Permissions.getAsync(Permissions.CALENDAR);
        if (permission.status !== 'granted') {
            permission = await Permissions.askAsync(Permissions.CALENDAR);
            if (permission.status !== 'granted') {
                Alert.alert('Permission not granted to show notifications');
            }
        }
        return permission;
    }

    async presentLocalNotification(date) {
        await this.obtainNotificationPermission();
        Notifications.presentLocalNotificationAsync({
            title: 'Your Reservation',
            body: 'Reservation for ' + date + ' requested',
            ios: {
                sound: true
            },
            android: {
                sound: true,
                vibrate: true,
                color: '#512DA8'
            }
        });
    }

    async getDefaultCalendarSource() {
        const calendars = await Calendar.getCalendarsAsync();
        const defaultCalendars = calendars.filter(each => each.source.name === 'Default');
        return defaultCalendars[0].source;
    }

    async addReservationToCalendar(date) {
        await this.obtainCalendarPermission();

        const defaultCalendarSource =
            Platform.OS === 'ios'
                ? await getDefaultCalendarSource()
                : { isLocalAccount: true, name: 'Expo Calendar' };


        let dateMs = Date.parse(date)
        let startDate = new Date(dateMs)
        let endDate = new Date(dateMs + 2 * 60 * 60 * 1000)

        await Calendar.createCalendarAsync({
            title: 'Test Reservation',
            color: '#512DA8',
            entityType: Calendar.EntityTypes.EVENT,
            sourceId: defaultCalendarSource.id,
            source: defaultCalendarSource,
            name: 'Restauran Reservation',
            ownerAccount: 'personal',
            accessLevel: Calendar.CalendarAccessLevel.OWNER,
        }).then((id) => {
            Calendar.createEventAsync(id, {
                title: 'Table Reservation',
                startDate: startDate,
                endDate: endDate,
                timeZone: 'Asia/Hong_Kong',
                location:
                    '121, Clear Water Bay Road, Clear Water Bay, Kowloon, Hong Kong',
            }).catch((err) => console.log(err))
        })
            .catch((err) => console.log(err))

    }


    render() {
        return (
            <Animatable.View animation="zoomIn" duration={2000}>
                <View style={styles.formRow}>
                    <Text style={styles.formLabel} style={{ flex: 2 }}>Number of Guests</Text>
                    <Picker
                        style={styles.formItem}
                        selectedValue={this.state.guests}
                        onValueChange={(itemValue, itemIndex) => this.setState({ guests: itemValue })}>
                        <Picker.Item label="1" value="1" />
                        <Picker.Item label="2" value="2" />
                        <Picker.Item label="3" value="3" />
                        <Picker.Item label="4" value="4" />
                        <Picker.Item label="5" value="5" />
                        <Picker.Item label="6" value="6" />
                    </Picker>
                </View>
                <View style={styles.formRow}>
                    <Text style={styles.formLabel}>Smoking/Non-Smoking?</Text>
                    <Switch
                        style={styles.formItem}
                        value={this.state.smoking}
                        onTintColor='#512DA8'
                        onValueChange={(value) => this.setState({ smoking: value })}>
                    </Switch>
                </View>
                <View style={styles.formRow}>
                    <View style={styles.formItem} >
                        <Text style={styles.formLabel}>Date and Time</Text>
                    </View>


                    <View style={{
                        flex: 1,
                        flexDirection: 'column',
                        flexWrap: 'wrap',
                    }}>


                        <Text style={{ width: '90%', marginBottom: -10 }}> {Moment(this.state.date).format('lll')} </Text>
                        <Icon style={{ width: '10%' }}
                            containerStyle={{ paddingRight: 20, marginBottom: -30 }}
                            name='calendar'
                            type='font-awesome'
                            color='#512DA8'
                            onPress={() => this.toggleDateTimePicker()}
                        />
                    </View>



                    {this.state.showDateTimePicker && (<DateTimePickerModal
                        isVisible={this.state.showDateTimePicker}
                        is24Hour
                        display="default"
                        isDarkModeEnabled={false}
                        onConfirm={this.onChangeDateTimePicker}
                        onCancel={() => this.toggleDateTimePicker()}
                        date={this.state.date}
                        mode='datetime'
                        locale="fr-FR"
                        headerTextIOS="Select reservation date"
                        confirmTextIOS="Confirm"
                        cancelTextIOS="Cancel"

                    />)}



                </View>
                <View style={styles.formRow} style={{ padding: 30 }}>
                    <Button
                        onPress={() => this.handleReservation()}
                        title="Reserve"
                        color="#512DA8"

                    />
                </View>

                <Modal animationType={"slide"} transparent={false}
                    visible={this.state.showModal}
                    onDismiss={() => this.toggleModal()}
                    onRequestClose={() => this.toggleModal()}>
                    <View style={styles.modal}>
                        <Text style={styles.modalTitle}>Your Reservation</Text>
                        <Text style={styles.modalText}>Number of Guests: {this.state.guests}</Text>
                        <Text style={styles.modalText}>Smoking?: {this.state.smoking ? 'Yes' : 'No'}</Text>
                        <Text style={styles.modalText}>Date and Time: {Moment(this.state.date).format('lll')}</Text>

                        <Button
                            onPress={() => { this.toggleModal(); this.resetForm(); }}
                            color="#512DA8"
                            title="Close"
                        />
                    </View>
                </Modal>

            </Animatable.View>


        )
    }

}

const styles = StyleSheet.create({
    formRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        margin: 20

    },
    formLabel: {
        fontSize: 18,

    },
    formItem: {
        flex: 1
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    }
});

export default Reservation;