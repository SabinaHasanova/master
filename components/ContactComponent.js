import React, { Component } from 'react';
import { Card, Text, Icon } from 'react-native-elements';
import { StyleSheet,Button } from 'react-native';
import * as Animatable from 'react-native-animatable';
import * as MailComposer from 'expo-mail-composer';

class Contact extends Component {

    sendMail() {    
        MailComposer.composeAsync({
            recipients: ['h.s.sebine@gmail.com'],
            subject: 'Enquiry',
            body: 'To whom it may concerm'
        })
    }

    render() {
        return (
            <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                <Card >
                    <Card.Title>Contact Information</Card.Title>
                    <Card.Divider />
                    <Text style={styles.h4Style}>121, Clear Water Bay Road</Text>
                    <Text style={styles.h4Style}>Clear Water Bay, Kowloon</Text>
                    <Text style={styles.h4Style}>HONG KONG</Text>
                    <Text style={styles.h4Style}>Tel: +852 1234 5678</Text>
                    <Text style={styles.h4Style}>Fax: +852 8765 4321</Text>
                    <Text style={styles.h4Style}>Email:confusion@food.net</Text>
                    <Button
                        title='Send Email'
                        buttonStyle={{ backgroundColor: '#512DA8' }}
                        onPress={()=> this.sendMail()}
                        icon={
                            <Icon name='envelope-o'
                                type='font-awasome'
                                color='white'                               
                            />

                        }
                    />
                </Card>
            </Animatable.View>
        );
    }
}


const styles = StyleSheet.create({
    h4Style: {
        margin: 8,
        fontSize: 15,
        fontWeight: 'bold',
    }

});

export default Contact;