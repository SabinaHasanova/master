import React, { Component } from 'react';
import { View, Text, ScrollView, FlatList, StyleSheet, Button, Modal, Alert, PanResponder,Share } from 'react-native';
import { Card, Icon, Rating, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreators';
import * as Animatable from 'react-native-animatable';



const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        comments: state.comments,
        favorites: state.favorites
    }
}

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (comment) => dispatch(postComment(comment))
})



function RenderDish(props) {
    const dish = props.dish;

    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        if (dx < -200)
            return true;
        else
            return false;
    }

    const recognizeComment = ({ moveX, moveY, dx, dy }) => {
        if (dx > 200)
            return true;
        else
            return false;
    }
    handleViewRef = ref => this.view = ref;
    const panResponder = PanResponder.create({

        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {
            this.view.rubberBand(1000)
                .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'));
        },
        onPanResponderEnd: (e, gestureState) => {

            if (recognizeDrag(gestureState)) {
                Alert.alert(
                    'Add Favorite',
                    'Are you sure you wish to add ' + dish.name + ' to favorite?',
                    [
                        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                        { text: 'OK', onPress: () => { props.favorite ? console.log('Already favorite') : props.onPress() } },
                    ],
                    { cancelable: false }
                )
            }
            else if (recognizeComment(gestureState)) {
                props.onToggleModal();
            }
        }
    })

    const shareDish=(title,message,url)=>{
        Share.share({
            title:title,
            message:title+ ': '+ message+ ' ' + url ,
            url: url
        },{
            dialogTitle:'Share '+ title
        });
    }
    
    if (dish != null) {
        return (

            <Animatable.View animation="fadeInDown" duration={1000} delay={1000}
                ref={this.handleViewRef}
                {...panResponder.panHandlers}>
                <Card >
                    <Card.Image source={{ uri: baseUrl + dish.image }}>
                        <Card.FeaturedTitle style={{
                            textAlign: 'center',
                            marginTop: 50
                        }}>{dish.name}</Card.FeaturedTitle>
                    </Card.Image>

                    <Text style={{ margin: 10 }}>
                        {dish.description}
                    </Text>
                    <View style={{ alignSelf: 'center', flexDirection: 'row' }}>
                        <Icon
                            reverse
                            name={props.favorite ? 'heart' : 'heart-o'}
                            type='font-awesome'
                            color='#f50'
                            onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                        />
                        <Icon
                            raised
                            reverse
                            name={'pencil'}
                            type='font-awesome'
                            color='#512DA8'
                            onPress={() => props.onToggleModal()}
                        />
                        <Icon
                            raised
                            reverse
                            name={'share'}
                            type='font-awesome'
                            color='#51D2A8'
                            onPress={() => shareDish(dish.name,dish.description,baseUrl+ dish.image)}
                        />
                    </View>
                </Card>
            </Animatable.View>

        );
    }
    else {
        return (<View></View>)
    }
}



function RenderComments(props) {
    const comments = props.comments;
    const renderCommentItem = ({ item, index }) => {
        return (
            <View key={index} style={{ margin: 10 }}>
                <Text style={{ fontSize: 14 }}>{item.comment}</Text>
                <Rating
                    imageSize={14}
                    readonly
                    startingValue={item.rating}
                    style={{ paddingVertical: 10, alignItems: 'flex-start' }} />
                <Text style={{ fontSize: 12 }}>{'-- ' + item.author + ', ' + item.date} </Text>
            </View>
        );
    }

    return (
        <Animatable.View animation="fadeInUp" duration={1000} delay={1000}>
            <Card title='Comments' >
                <Card.Title>Comments</Card.Title>
                <Card.Divider />
                <FlatList
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={item => item.id.toString()}
                />
            </Card>
        </Animatable.View>
    )
}

function RenderAddComment(props) {
    return (
        <Modal animationType={"slide"} transparent={false}
            visible={props.showModal}
            onDismiss={() => props.onToggleModal()}
            onRequestClose={() => props.onToggleModal()}>
            <View style={styles.modal}>

                <Rating showRating fractions="{1}" startingValue="{3}"
                    onFinishRating={rating => props.onInputChange('rating', rating)}
                />
                <Input
                    placeholder='Author'
                    name="author"
                    onChangeText={value => props.onInputChange('author', value)}
                    leftIcon={
                        <Icon
                            name='user-o'
                            size={24}
                            type='font-awesome'
                        />
                    }
                />
                <Input
                    placeholder='Comment'
                    name="comment"
                    onChangeText={value => props.onInputChange('comment', value)}
                    leftIcon={
                        <Icon
                            name='comment-o'
                            type='font-awesome'
                            size={22}
                            color='black'
                        />
                    }
                />

                <View style={styles.modalButton}>
                    <Button
                        onPress={() => {
                            props.handleComment();
                            props.resetForm()
                        }}
                        color="#512DA8"
                        title="Submit" />

                </View>

                <View style={styles.modalButton}>
                    <Button
                        onPress={() => {
                            props.onToggleModal();
                            props.resetForm();
                        }}
                        color="grey"
                        title="Cancel"
                    />
                </View>

            </View>
        </Modal>
    )
}

class DishDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            author: '',
            comment: '',
            rating: 3,

        }
    }
    static navigationOptions = {
        title: 'Dish Details'
    }
    onMarkFavorite(dishId) {

        this.props.postFavorite(dishId);
    }

    handleComment(dishId) {

        let maxid = this.props.comments.comments.length;
        let comment = {
            id: maxid,
            dishId,
            author: this.state.author,
            comment: this.state.comment,
            rating: this.state.rating,
            date: new Date().toISOString()
        }
        this.props.postComment(comment);


        this.toggleModal();

    }

    toggleModal = () => {
        this.setState({ showModal: !this.state.showModal });
    }

    resetForm() {
        this.setState({
            showModal: false,
            author: '',
            comment: '',
            rating: 3,
        });
    }

    onInputChange = (name, value) => {

        this.setState(state => ({
            ...state,
            [name]: value
        }))

    }


    render() {
        const { dishId } = this.props.route.params;
        return (
            <ScrollView>
                <RenderDish dish={this.props.dishes.dishes[dishId]}
                    favorite={this.props.favorites.some(el => el === dishId)}
                    onPress={() => this.onMarkFavorite(dishId)}
                    onToggleModal={() => this.toggleModal()} />

                <RenderComments
                    comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)} />

                <RenderAddComment
                    onToggleModal={() => this.toggleModal()}
                    showModal={this.state.showModal}
                    handleComment={() => this.handleComment(dishId)}
                    onInputChange={this.onInputChange}
                    resetForm={() => this.resetForm()}

                />

            </ScrollView>
        );
    }

}

const styles = StyleSheet.create({

    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    },
    modalButton: {
        padding: 10,

    }
});


export default connect(mapStateToProps, mapDispatchToProps)(DishDetail);